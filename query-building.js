const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  // Update
  // const room = await Room.findById('621b390c80fda30f7a2ac60a')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('--------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({})
  console.log(JSON.stringify(building))
}
main().then(() => {
  console.log('Finish')
})
